<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Mahasiswa;

class Berkas extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_mahasiswa',
        'cv',
        'sertifikat',
        'portofolio'
    ];
    public function mahasiswa(){
        return $this->belongsTo(Mahasiswa::class, 'id_mahasiswa', 'id');
    }
}
