<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Mitra;

class Posisi extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_mitra',
        'nama',
        'deskripsi',
        'kriteria',
        'benefit',
        'status',
        'tanggal_buka',
        'periode',
        'tanggal_tutup',
        'kuota'
    ];
    public function mitra(){
        return $this->belongsTo(Mitra::class, 'id_mitra', 'id');
    }
    public function applies()
    {
        return $this->hasMany(Apply::class, 'id_posisi');
    }
}
