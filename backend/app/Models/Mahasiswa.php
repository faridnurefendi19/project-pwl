<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Berkas;

class Mahasiswa extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'id_user',
        'nim',
        'jurusan',
        'fakultas',
        'no_wa',
        'angkatan',
        'alamat',
    ];

    public function berkas(){
        return $this->hasMany(Berkas::class, 'id_mahasiswa', 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
    public function applies()
    {
        return $this->hasMany(Apply::class, 'id_mahasiswa');
    }
}
