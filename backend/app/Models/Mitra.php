<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Posisi;

class Mitra extends Model
{
    use HasFactory;
    protected $fillable = ['id_user','nama', 'jenis', 'lingkup', 'status', 'logo','deskripsi', 'visi', 'misi','emailMitra'];
    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
    public function posisi(){
        return $this->hasMany(Posisi::class, 'id_mitra', 'id');
    }
}
