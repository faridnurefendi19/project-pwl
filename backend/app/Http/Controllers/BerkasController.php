<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Berkas;

class BerkasController extends Controller
{

    public function store(Request $request)
{
    // Mengambil user yang sedang login dengan guard 'api'
    $user = auth()->guard('api')->user();

    // Jika user tidak ditemukan, kembalikan respons 'Unauthorized'
    if (!$user) {
        return response()->json(['error' => 'Unauthorized'], 401);
    }

    // Validasi input request
    $request->validate([
        'id_mahasiswa' => '',
        'cv' => 'nullable|file|mimes:pdf,doc,docx',
        'sertifikat' => 'nullable|file|mimes:pdf,doc,docx,jpg,jpeg,png',
        'portofolio' => 'nullable|file|mimes:pdf,doc,docx,jpg,jpeg,png',
    ]);

    // Menyimpan file CV
    $cvUrl = null;
    if ($request->hasFile('cv')) {
        $cvFile = $request->file('cv');
        $cvOriginalName = $cvFile->getClientOriginalName();
        $cvFile->move(storage_path('app/public/lampiran/'), $cvOriginalName);
        $cvUrl = $cvOriginalName;
    }

    // Menyimpan file sertifikat
    $sertifikatUrl = null;
    if ($request->hasFile('sertifikat')) {
        $sertifikatFile = $request->file('sertifikat');
        $sertifikatOriginalName = $sertifikatFile->getClientOriginalName();
        $sertifikatFile->move(storage_path('app/public/lampiran/'), $sertifikatOriginalName);
        $sertifikatUrl = $sertifikatOriginalName;
    }

    // Menyimpan file portofolio
    $portofolioUrl = null;
    if ($request->hasFile('portofolio')) {
        $portofolioFile = $request->file('portofolio');
        $portofolioOriginalName = $portofolioFile->getClientOriginalName();
        $portofolioFile->move(storage_path('app/public/lampiran/'), $portofolioOriginalName);
        $portofolioUrl = $portofolioOriginalName;
    }

    $file = new Berkas();
    $file->id_mahasiswa = $user->mahasiswa->id;

    // Menyimpan path file ke database
    $file->cv = $cvUrl;
    $file->sertifikat = $sertifikatUrl;
    $file->portofolio = $portofolioUrl;

    // Menyimpan instance ke database
    $file->save();

    // Mengembalikan respons JSON dengan pesan sukses
    return response()->json([
        'message' => 'Files uploaded successfully!',
        'data' => $file
    ], 201);
}




    public function getBerkas()
    {
        $user = auth()->guard('api')->user();

        if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $berkas = Berkas::with('mahasiswa')->whereHas('mahasiswa', function ($query) use ($user) {
            $query->where('id', $user->mahasiswa->id);
        })
            ->get();

        if ($berkas->isEmpty()) {
            return response()->json(['error' => 'Mitra not found'], 404);
        }

        return response()->json(['posisi' => $berkas], 200);
    }
}
