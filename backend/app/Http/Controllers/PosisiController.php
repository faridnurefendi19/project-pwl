<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Posisi;
use Illuminate\Support\Facades\Validator;

class PosisiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function all(){
        $user = auth()->guard('api')->user();

        if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $posisi = Posisi::with('mitra')->get();

        if (!$posisi) {
            return response()->json(['error' => 'Mitra not found'], 404);
        }
        return response()->json(['posisi' => $posisi], 200);

    }
    public function posisiMitra() {
        $user = auth()->guard('api')->user();

        if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        // Asumsikan bahwa $user->id adalah id_mitra
        $posisi = Posisi::with('mitra')->whereHas('mitra', function($query) use ($user) {
            $query->where('id_mitra', $user->mitra->id);
        })
        ->get();

        if ($posisi->isEmpty()) {
            return response()->json(['error' => 'Mitra not found'], 404);
        }

        return response()->json(['posisi' => $posisi], 200);
    }

    public function showPosisi($id)
    {
        $user = auth()->guard('api')->user();

        if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $posisi = Posisi::with('mitra')->find($id);

        if (!$posisi) {
            return response()->json(['error' => 'Mitra not found'], 404);
        }
        return response()->json(['posisi' => $posisi], 200);
    }


    public function create(Request $request)
    {
        $user = auth()->guard('api')->user();

        if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $validator = Validator::make($request->all(), [
            'nama' => 'required|string|max:255',
            'deskripsi' => 'required|string',
            'kriteria' => 'required|string',
            'benefit' => 'required|string',
            'status' => 'nullable|string|max:255',
            'tanggal_buka' => 'required',
            'periode' => 'required|string|max:255',
            'tanggal_tutup' => 'required',
            'kuota' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $id_mitra = $user->mitra->id;

        try {
            $posisi = Posisi::create([
                'id_mitra' => $id_mitra,
                'nama' => $request->input('nama'),
                'deskripsi' => $request->input('deskripsi'),
                'kriteria' => $request->input('kriteria'),
                'benefit' => $request->input('benefit'),
                'tanggal_buka' => $request->input('tanggal_buka'),
                'periode' => $request->input('periode'),
                'tanggal_tutup' => $request->input('tanggal_tutup'),
                'kuota' => $request->input('kuota'),
            ]);

            return response()->json(['message' => 'Posisi created successfully', 'posisi' => $posisi], 201);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Creation failed. Please try again.'], 500);
        }
    }

    public function update(Request $request, $id)
    {
        $user = auth()->guard('api')->user();

        if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $validator = Validator::make($request->all(), [
            'nama' => 'sometimes|required|string|max:255',
            'deskripsi' => 'sometimes|required|string',
            'kriteria' => 'sometimes|required|string',
            'benefit' => 'sometimes|required|string',
            'status' => 'sometimes|required|string|max:255',
            'tanggal_buka' => 'sometimes|required',
            'periode' => 'sometimes|required|string|max:255',
            'tanggal_tutup' => 'sometimes|required',
            'kuota' => 'sometimes|required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $id_mitra = $user->mitra->id;

        try {
            $posisi = Posisi::findOrFail($id);

            if ($request->has('id_mitra')) {
                $posisi->id_mitra = $id_mitra;
            }
            if ($request->has('nama')) {
                $posisi->nama = $request->input('nama');
            }
            if ($request->has('deskripsi')) {
                $posisi->deskripsi = $request->input('deskripsi');
            }
            if ($request->has('kriteria')) {
                $posisi->kriteria = $request->input('kriteria');
            }
            if ($request->has('benefit')) {
                $posisi->benefit = $request->input('benefit');
            }
            if ($request->has('status')) {
                $posisi->status = $request->input('status');
            }
            if ($request->has('tanggal_buka')) {
                $posisi->tanggal_buka = $request->input('tanggal_buka');
            }
            if ($request->has('periode')) {
                $posisi->periode = $request->input('periode');
            }
            if ($request->has('tanggal_tutup')) {
                $posisi->tanggal_tutup = $request->input('tanggal_tutup');
            }
            if ($request->has('kuota')) {
                $posisi->kuota = $request->input('kuota');
            }

            $posisi->save();

            return response()->json(['message' => 'Posisi updated successfully', 'posisi' => $posisi], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Update failed. Please try again.'], 500);
        }
    }

    public function destroy($id)
    {
        $user = auth()->guard('api')->user();

        if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        try {
            $posisi = Posisi::findOrFail($id);
            $posisi->delete();

            return response()->json(['message' => 'Posisi deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Deletion failed. Please try again.'], 500);
        }
    }
}
