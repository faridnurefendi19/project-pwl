<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Mitra;
use App\Models\Mahasiswa;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Register a new user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'role' => 'required|string|in:admin,mitra,mahasiswa',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string',

            // Validasi khusus untuk mitra
            'nama' => 'required_if:role,mitra|string|max:255',
            'deskripsi' => 'required_if:role,mitra',
            'visi' => 'required_if:role,mitra',
            'misi' => 'required_if:role,mitra',
            'emailMitra' => 'required_if:role,mitra|string|max:255',
            'jenis' => 'required_if:role,mitra|string|max:255',
            'lingkup' => 'required_if:role,mitra|string|max:255',
            'status' => 'nullable_if:role,mitra|string|max:255',
            'logo' => 'required_if:role,mitra|string|max:255',

            // Validasi khusus untuk mahasiswa
            'nim' => 'required_if:role,mahasiswa|string|max:255|unique:mahasiswas',
            'jurusan' => 'required_if:role,mahasiswa|string|max:255',
            'fakultas' => 'required_if:role,mahasiswa|string|max:255',
            'no_wa' => 'required_if:role,mahasiswa|string|max:255',
            'angkatan' => 'required_if:role,mahasiswa|string|max:255',
            'alamat' => 'required_if:role,mahasiswa|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        DB::beginTransaction();

        try {
            $user = User::create([
                'name' => $request->input('name'),
                'role' => $request->input('role'),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
            ]);

            if ($user->role === 'mitra') {
                $mitra = Mitra::create([
                    'id_user' => $user->id,
                    'nama' => $request->input('nama'),
                    'deskripsi' => $request->input('deskripsi'),
                    'visi' => $request->input('visi'),
                    'misi' => $request->input('misi'),
                    'emailMitra' => $request->input('emailMitra'),
                    'jenis' => $request->input('jenis'),
                    'lingkup' => $request->input('lingkup'),
                    'logo' => $request->input('logo'),
                ]);
            }

            if ($user->role === 'mahasiswa') {
                $mahasiswa = Mahasiswa::create([
                    'id_user' => $user->id,
                    'nim' => $request->input('nim'),
                    'jurusan' => $request->input('jurusan'),
                    'fakultas' => $request->input('fakultas'),
                    'no_wa' => $request->input('no_wa'),
                    'angkatan' => $request->input('angkatan'),
                    'alamat' => $request->input('alamat'),
                ]);
            }

            DB::commit();

            return response()->json(['message' => 'User registered successfully', 'user' => $user], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => 'Registration failed. Please try again.'], 500);
        }
    }


    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        // Ambil informasi pengguna setelah berhasil login
        $user = auth()->user();

        // Masukkan informasi role ke dalam respons
        return $this->respondWithToken($token, $user->role);
    }

    private function respondWithToken($token, $role = null)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'role' => $role, // Sertakan role dalam respons jika diperlukan
        ]);
    }


    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = auth()->user();

        if ($user->role == 'mahasiswa') {
            $user->mahasiswa = $user->mahasiswa; // Mengakses relasi mahasiswa untuk memuatnya
        } elseif ($user->role == 'mitra') {
            $user->mitra = $user->mitra; // Mengakses relasi mitra untuk memuatnya
        }

        return response()->json($user);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */

}
