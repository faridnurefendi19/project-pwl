<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Apply;

class ApplyController extends Controller
{
    //
    public function store(Request $request)
    {
        $user = auth()->user();

        if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $validatedData = $request->validate([
            'id_posisi' => 'required|exists:posisis,id',
            'status' => 'nullable|string'
        ]);

        $apply = Apply::create([
            'id_posisi' => $validatedData['id_posisi'],
            'id_mahasiswa' => $user->mahasiswa->id,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Apply created successfully',
            'data' => $apply
        ], 201);
    }
}
