<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;

class MahasiswaController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function showMahasiswa($id)
    {
        $user = auth()->guard('api')->user();

        if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $mahasiswa = Mahasiswa::with('user')->find($id);

        if (!$mahasiswa) {
            return response()->json(['error' => 'Mitra not found'], 404);
        }
        return response()->json(['mahasiswa' => $mahasiswa], 200);
    }

    public function update(Request $request)
    {
        $user = auth()->guard('api')->user();

        if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $request->validate([
            'nim' => 'string|max:255',
            'jurusan' => 'string|max:255',
            'fakultas' => 'string|max:255',
            'no_wa' => 'string|max:20',
            'angkatan' => 'nullable|string|max:255',
            'alamat' => 'string|max:255',
        ]);

        $data = Mahasiswa::where('id_user', $user->id)->first();
        if (!$data) {
            return response()->json(['error' => 'Anda belum mendaftar.'], 404);
        }

        // Menggunakan metode has() untuk memeriksa keberadaan dan mengatur nilai atribut
        if ($request->has('nim')) {
            $data->nim = $request->nim;
        }
        if ($request->has('jurusan')) {
            $data->jurusan = $request->jurusan;
        }
        if ($request->has('fakultas')) {
            $data->fakultas = $request->fakultas;
        }
        if ($request->has('no_wa')) {
            $data->no_wa = $request->no_wa;
        }
        if ($request->has('angkatan')) {
            $data->angkatan = $request->angkatan;
        }
        if ($request->has('alamat')) {
            $data->alamat = $request->alamat;
        }

        // Menyimpan perubahan ke dalam database
        $data->save();

        return response()->json(['message' => 'Data updated successfully']);
    }
}
