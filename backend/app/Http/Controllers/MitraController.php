<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Mitra;
use Illuminate\Http\Request;

class MitraController extends Controller
{

    public function showMitra()
    {
        $user = auth()->user();

        if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        // Cari Mitra berdasarkan ID pengguna yang sedang login
        $mitra = Mitra::with('user')->where('id_user', $user->id)->first();

        // Jika Mitra tidak ditemukan, kembalikan respons 404
        if (!$mitra) {
            return response()->json(['error' => 'Mitra not found'], 404);
        }

        // Kembalikan detail Mitra beserta pengguna terkait
        return response()->json(['mitra' => $mitra], 200);
    }


    public function update(Request $request)
    {
        $user = auth()->guard('api')->user();

        if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $request->validate([
            'nama' => 'string|max:255',
            'jenis' => 'string|max:255',
            'deskripsi' => '',
            'visi' => '',
            'misi' => '',
            'emailMitra' => 'string|max:255',
            'lingkup' => 'string|max:255',
            'logo' => 'string|max:255',
        ]);

        $data = Mitra::where('id_user', $user->id)->first();
        if (!$data) {
            return response()->json(['error' => 'Anda belum mendaftar.'], 404);
        }
        if ($request->has('nama')) {
            $data->nama = $request->nama;
        }
        if ($request->has('deskripsi')) {
            $data->deskripsi = $request->deskripsi;
        }
        if ($request->has('visi')) {
            $data->visi = $request->visi;
        }
        if ($request->has('misi')) {
            $data->misi = $request->misi;
        }
        if ($request->has('emailMitra')) {
            $data->emailMitra = $request->emailMitra;
        }
        if ($request->has('jenis')) {
            $data->jenis = $request->jenis;
        }
        if ($request->has('lingkup')) {
            $data->lingkup = $request->lingkup;
        }
        if ($request->has('logo')) {
            $data->logo = $request->logo;
        }


        $data->save();

        return response()->json(['message' => 'Data updated successfully']);
    }

    public function updateStatus(Request $request)
    {
        $user = auth()->guard('api')->user();

        if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $mitra = Mitra::where('id_user', $user->id)->first();
        if (!$mitra) {
            return response()->json(['error' => 'Anda belum mendaftar.'], 404);
        }
        $mitra->status = 'proses';
        $mitra->save();
        return response()->json(['message' => 'Data updated successfully']);
    }
}
