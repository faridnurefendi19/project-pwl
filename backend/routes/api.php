<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\MitraController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\PosisiController;
use App\Http\Controllers\BerkasController;
use App\Http\Controllers\ApplyController;
use App\Models\Apply;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', [AuthController::class,'login']);
    Route::post('register', [AuthController::class,'register']);
    Route::post('logout', [AuthController::class,'logout']);
    Route::post('refresh', [AuthController::class,'refresh']);
    Route::get('me', [AuthController::class,'me']);

});

Route::group([
    'middleware' => 'api',
    'prefix' => 'mitra'
], function ($router) {
    Route::get('detail', [MitraController::class,'showMitra']);
    Route::put('update', [MitraController::class,'update']);
    Route::put('updateStatus', [MitraController::class,'updateStatus']);

});

Route::group([
    'middleware' => 'api',
    'prefix' => 'mahasiswa'
], function ($router) {
    Route::get('detail/{id}', [MahasiswaController::class,'showMahasiswa']);
    Route::put('update', [MahasiswaController::class,'update']);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'posisi'
], function ($router) {
    Route::get('all', [PosisiController::class,'all']);
    Route::get('posisi-mitra', [PosisiController::class,'posisiMitra']);
    Route::get('detail/{id}', [PosisiController::class,'showPosisi']);
    Route::post('tambah', [PosisiController::class,'create']);
    Route::put('update/{id}', [PosisiController::class,'update']);
    Route::delete('delete/{id}', [PosisiController::class,'destroy']);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'berkas'
], function ($router) {
    Route::get('', [PosisiController::class,'all']);
    Route::get('get-berkas', [BerkasController::class,'getBerkas']);
    Route::get('detail/{id}', [PosisiController::class,'showPosisi']);
    Route::post('tambah', [BerkasController::class,'store']);
    Route::put('update/{id}', [PosisiController::class,'update']);
    Route::delete('delete/{id}', [PosisiController::class,'destroy']);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'apply'
], function ($router) {
    Route::get('', [ApplyController::class,'all']);
    Route::get('detail/{id}', [ApplyController::class,'showPosisi']);
    Route::post('tambah', [ApplyController::class,'store']);
    Route::put('update/{id}', [ApplyController::class,'update']);
    Route::delete('delete/{id}', [ApplyController::class,'destroy']);
});



