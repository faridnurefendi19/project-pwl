<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;


class AuthTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test registration endpoint.
     *
     * @return void
     */
    public function testUserRegistration()
    {
        $response = $this->postJson('/api/auth/register', [
            'name' => 'John Doe',
            'role' => 'mitra',
            'email' => 'john@example.com',
            'password' => 'password',
            'nama' => 'Example Mitra',
            'jenis' => 'Type',
            'lingkup' => 'Area',
            'status' => 'Active',
            'logo' => 'logo',
        ]);

        $response->assertStatus(201)
            ->assertJson([
                'message' => 'User registered successfully',
            ]);
    }

    /**
     * Test login endpoint.
     *
     * @return void
     */



    public function testLogin()
    {
        $response = $this->postJson('/api/auth/register', [
            'name' => 'John Doe',
            'role' => 'mitra',
            'email' => 'john@example.com',
            'password' => 'password',
            'nama' => 'Example Mitra',
            'jenis' => 'Type',
            'lingkup' => 'Area',
            'status' => 'Active',
            'logo' => 'logo',
        ]);
        $credentials = [
            'email' => 'john@example.com',
            'password' => 'password',
        ];

        $response = $this->postJson('/api/auth/login', $credentials);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'token_type',
                'expires_in',
            ])
            ->assertJson([
                'token_type' => 'bearer',
                'expires_in' => 3600,
            ]);
    }


    /**
     * Test get authenticated user endpoint.
     *
     * @return void
     */
    public function testLoginAndTestMe()
    {
        // Register a user
        $registerResponse = $this->postJson('/api/auth/register', [
            'name' => 'John Doe',
            'role' => 'mitra',
            'email' => 'john@example.com',
            'password' => 'password',
            'nama' => 'Example Mitra',
            'jenis' => 'Type',
            'lingkup' => 'Area',
            'status' => 'Active',
            'logo' => 'logo',
        ]);

        // Assert registration was successful
        $registerResponse->assertStatus(201);

        // Attempt to login
        $credentials = [
            'email' => 'john@example.com',
            'password' => 'password',
        ];
        $loginResponse = $this->postJson('/api/auth/login', $credentials);

        $loginResponse->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'token_type',
                'expires_in',
            ])
            ->assertJson([
                'token_type' => 'bearer',
                'expires_in' => 3600,
            ]);

        $accessToken = $loginResponse['access_token'];
        echo "Access Token: " . $accessToken;

        $testMeResponse = $this->withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
        ])->postJson('/api/auth/me');


        // Assert test_me endpoint is accessible
        $testMeResponse->assertStatus(200)
            ->assertJson([]);
    }




    /**
     * Test logout endpoint.
     *
     * @return void
     */
    public function test_logout()
    {
        $registerResponse = $this->postJson('/api/auth/register', [
            'name' => 'John Doe',
            'role' => 'mitra',
            'email' => 'john@example.com',
            'password' => 'password',
            'nama' => 'Example Mitra',
            'jenis' => 'Type',
            'lingkup' => 'Area',
            'status' => 'Active',
            'logo' => 'logo',
        ]);

        // Assert registration was successful
        $registerResponse->assertStatus(201);

        // Attempt to login
        $credentials = [
            'email' => 'john@example.com',
            'password' => 'password',
        ];
        $loginResponse = $this->postJson('/api/auth/login', $credentials);

        $loginResponse->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'token_type',
                'expires_in',
            ])
            ->assertJson([
                'token_type' => 'bearer',
                'expires_in' => 3600,
            ]);

        $accessToken = $loginResponse['access_token'];
        echo "Access Token: " . $accessToken;

        $testMeResponse = $this->withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
        ])->postJson('/api/auth/logout');


        // Assert test_me endpoint is accessible
        $testMeResponse->assertStatus(200)
            ->assertJson([]);
    }

    /**
     * Test refresh token endpoint.
     *
     * @return void
     */
    public function test_refresh()
    {
        $registerResponse = $this->postJson('/api/auth/register', [
            'name' => 'John Doe',
            'role' => 'mitra',
            'email' => 'john@example.com',
            'password' => 'password',
            'nama' => 'Example Mitra',
            'jenis' => 'Type',
            'lingkup' => 'Area',
            'status' => 'Active',
            'logo' => 'logo',
        ]);

        // Assert registration was successful
        $registerResponse->assertStatus(201);

        // Attempt to login
        $credentials = [
            'email' => 'john@example.com',
            'password' => 'password',
        ];
        $loginResponse = $this->postJson('/api/auth/login', $credentials);

        $loginResponse->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'token_type',
                'expires_in',
            ])
            ->assertJson([
                'token_type' => 'bearer',
                'expires_in' => 3600,
            ]);

        $accessToken = $loginResponse['access_token'];
        echo "Access Token: " . $accessToken;

        $testMeResponse = $this->withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
        ])->postJson('/api/auth/refresh');


        // Assert test_me endpoint is accessible
        $testMeResponse->assertStatus(200)
            ->assertJson([]);
    }
}
