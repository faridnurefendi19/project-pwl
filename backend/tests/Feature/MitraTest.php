<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;


class MitraTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test registration endpoint.
     *
     * @return void
     */
    public function testDetailMahasiswa()
    {
        $registerResponse = $this->postJson('/api/auth/register', [
            'name' => 'John Doe',
            'role' => 'mitra',
            'email' => 'john@example.com',
            'password' => 'password',
            'nama' => 'Example Mitra',
            'jenis' => 'Type',
            'lingkup' => 'Area',
            'status' => 'Active',
            'logo' => 'logo',
        ]);

        // Assert registration was successful
        $registerResponse->assertStatus(201);

        // Attempt to login
        $credentials = [
            'email' => 'john@example.com',
            'password' => 'password',
        ];
        $loginResponse = $this->postJson('/api/auth/login', $credentials);

        $loginResponse->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'token_type',
                'expires_in',
            ])
            ->assertJson([
                'token_type' => 'bearer',
                'expires_in' => 3600,
            ]);

        $accessToken = $loginResponse['access_token'];
        echo "Access Token: " . $accessToken;

        $user = User::where('email', 'john@example.com')->first();


        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
        ])->postJson('/api/mitra/detail/' . $mitra->id);
        $response->assertStatus(201)
            ->assertJson([
                'message' => 'User registered successfully',
            ]);
    }

    /**
     * Test login endpoint.
     *
     * @return void
     */






    /**
     * Test get authenticated user endpoint.
     *
     * @return void
     */





    /**
     * Test logout endpoint.
     *
     * @return void
     */


    /**
     * Test refresh token endpoint.
     *
     * @return void
     */

}
