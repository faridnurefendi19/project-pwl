<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('posisis', function (Blueprint $table) {
            $table->id();
            $table->string('id_mitra');
            $table->string('nama');
            $table->longText('deskripsi');
            $table->longText('kriteria');
            $table->longText('benefit');
            $table->string('tanggal_buka');
            $table->string('periode');
            $table->string('tanggal_tutup');
            $table->string('status')->default('open');
            $table->integer('kuota');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('posisis');
    }
};
