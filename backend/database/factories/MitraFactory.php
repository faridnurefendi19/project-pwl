<?php

namespace Database\Factories;

use App\Models\Mitra;
use Illuminate\Database\Eloquent\Factories\Factory;

class MitraFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Mitra::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nama' => $this->faker->company,
            'jenis' => $this->faker->word,
            'id_user' => \App\Models\User::factory(), // Menggunakan factory User untuk mendapatkan id_user
            'lingkup' => $this->faker->sentence,
            'status' => $this->faker->word,
            'logo' => $this->faker->imageUrl(),
        ];
    }
}
