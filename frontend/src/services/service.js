import api from '../api/api';

export const submitForm = async (formData) => {
    try {
        const response = await api.post('/auth/register', formData);
        return response.data;
    } catch (error) {
        console.error('Error submitting form:', error);
        throw error;
    }
};

export const loginForm = async (formData) => {
    try {
        const response = await api.post('/auth/login', formData);
        return response.data;
    } catch (error) {
        console.error('Error submitting login form:', error);
        throw error;
    }
};


export const getProfile = async (token) => {
    try {
        const response = await api.get('/auth/me', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });

        return response.data;
    } catch (error) {
        console.error('Error fetching profile:', error);
        throw error;
    }
};

export const logout = async () => {
    try {
        const token = localStorage.getItem('access_token');
        if (!token) {
            throw new Error('Access token not found');
        }

        await api.post('/auth/logout', {}, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });

        localStorage.removeItem('access_token');
    } catch (error) {
        console.error('Error logging out:', error);
        throw error;
    }
};

export const getMitraDetails = async (token) => {
    try {
        const response = await api.get('/mitra/detail', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });

        return response.data;
    } catch (error) {
        console.error('Error fetching profile:', error);
        throw error;
    }
};

export const editMitra = async (token, formData) => {
    try {
        const response = await api.put('/mitra/update', formData, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });

        return response.data;
    } catch (error) {
        console.error('Error fetching profile:', error);
        throw error;
    }
};

export const ajukanMitra = async (token) => {
    try {
        const response = await api.put('/mitra/updateStatus', token, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error update status:', error);
        throw error;
    }
};

export const storePosisi = async (token, formData) => {
    try {
        const response = await api.post('/posisi/tambah', formData, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error submitting form:', error);
        throw error;
    }
};

export const showPosisi = async (token, id) => {
    try {
        const response = await api.get(`/posisi/detail/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error fetching posisi:', error);
        throw error;
    }
};

export const editPosisi = async (token, id, formData) => {
    try {
        const response = await api.put(`/posisi/update/${id}`,formData, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error fetching posisi:', error);
        throw error;
    }
};


export const getPosisiByMitra = async (token) => {
    try {
        const response = await api.get('/posisi/posisi-mitra', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error submitting form:', error);
        throw error;
    }
};
export const getPosisiAll = async (token) => {
    try {
        const response = await api.get('/posisi/all', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error submitting form:', error);
        throw error;
    }
};

export const editMahasiswa = async (token, formData) => {
    try {
        const response = await api.put('/mahasiswa/update', formData, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });

        return response.data;
    } catch (error) {
        console.error('Error fetching profile:', error);
        throw error;
    }
};

export const addBerkas = async (token, formData) => {
    try {
        const response = await api.put('/berkas/tambah', formData, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });

        return response.data;
    } catch (error) {
        console.error('Error fetching profile:', error);
        throw error;
    }
};

export const getBerkas = async (token) => {
    try {
        const response = await api.get('/berkas/get-berkas',  {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });

        return response.data;
    } catch (error) {
        console.error('Error fetching profile:', error);
        throw error;
    }
};

export const storeBerkas = async (token, formData) => {
    try {
        const response = await api.post('/berkas/tambah', formData, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error submitting form:', error);
        throw error;
    }
};

