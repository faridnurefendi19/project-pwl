import {
    createWebHistory,
    createRouter
} from "vue-router";
import HomePage from "./pages/user/HomePage.vue";
import IndexRecruitment from "./pages/user/recruitmen/IndexPage.vue";
import ShowRecruitment from "./pages/user/recruitmen/ShowPage.vue";
import HeaderProfil from "./components/HeaderProfil.vue";
import ProfilPage from "./pages/user/profil/IndexPage.vue";
import EditProfilPage from "./pages/user/profil/EditPage.vue";
import DokumenPage from "./pages/user/dokumen/IndexPage.vue";
import TambahDokumen from "./pages/user/dokumen/CreateDokumen.vue";
import AktivitasPage from "./pages/user/aktivitas/IndexPage.vue";
import DashboardPage from "./pages/admin/DashboardPage.vue";
import MasterAdmin from "./pages/Layout/MasterAdmin.vue";
import MasterUser from "./pages/Layout/MasterUser.vue";
import PosisiIndex from "./pages/admin/posisi/IndexTable.vue";
import PosisiCreate from "./pages/admin/posisi/CreatePosisi.vue";
import PosisiEdit from "./pages/admin/posisi/EditPosisi.vue";
import PosisiShow from "./pages/admin/posisi/ShowPosisi.vue";
import KemitraanShow from "./pages/admin/kemitraan/ShowKemitraan.vue";
import KemitraanEdit from "./pages/admin/kemitraan/EditKemitraan.vue";
import Login from "./pages/auth/LoginPage.vue";
import Register from "./pages/auth/RegisterPage.vue";

const routes = [{
        path: "/admin",
        component: MasterAdmin,
        children: [{
                path: 'dashboard',
                component: DashboardPage
            },
            {
                path: 'posisi',
                children: [{
                        path: '',
                        component: PosisiIndex,
                    },
                    {
                        path: 'add',
                        component: PosisiCreate,
                    },
                    {
                        path: 'edit/:id',
                        component: PosisiEdit
                    },
                    {
                        path: 'detail/:id',
                        component: PosisiShow
                    },
                ]
            },
            {
                path: 'kemitraan',
                children: [{
                        path: '',
                        component: KemitraanShow,
                    },
                    {
                        path: 'edit',
                        component: KemitraanEdit
                    },
                ]
            },
        ],
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/user",
        component: MasterUser,
        children: [{
                path: 'home',
                component: HomePage
            },
            {
                path: 'recruitment',
                children:[
                    {
                        path: '',
                        component: IndexRecruitment,
                    },
                    {
                        path: 'detail/:id',
                        component: ShowRecruitment,
                    },
                ]
            },

            {
                path: 'profil',
                component: HeaderProfil,
                children: [{
                        path: 'data',
                        children:[
                            {
                                path: '',
                                component: ProfilPage,
                            },
                            {
                                path: 'edit',
                                component: EditProfilPage,
                            },
                        ]
                    },
                    {
                        path: 'dokumen',
                        children:[
                            {
                                path:'',
                                component:DokumenPage
                            },
                            {
                                path:'add',
                                component:TambahDokumen
                            },
                        ]
                    },
                    {
                        path: 'aktivitas',
                        component: AktivitasPage,
                    }
                ]
            },
        ],
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/login",
        component: Login
    },
    {
        path: "/register",
        component: Register
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

router.beforeEach((to, from, next) => {
    const token = localStorage.getItem('access_token');

    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!token) {
            next('/login');
        } else {
            next();
        }
    } else {
        next();
    }
});

export default router;